<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html dir="ltr" lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=yes, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="ヤスイ家具株式会社 | イケてる家具をお手頃な値段であなたに">
    <meta name="keywords" content="">
    <title>ヤスイ家具株式会社 | やすい家具ならヤスイ家具</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/purchase.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header id="header">
        <div class="inner">
            <!-- ロゴ -->
            <div class="logo">
                <a href="index.html">Yasui Furniture</a>
                <div class="boxContainer">
                    <div class="box">
                        <h1>商品の選択</h1>
                    </div>
                    <div class="box">user1（香川真司）</div>
                </div>

            </div>
            <!-- / ロゴ -->
            <!-- メインナビゲーション -->
            <nav id="mainNav">
                <a class="menu" id="menu"><span>MENU</span></a>
                <div class="panel">
                    <ul>
                        <li id="list" class="active">
                            <a href="list.html">
                                <p>商品購入</p>
                                <p class="panel-caption">Purchase</p>
                            </a>
                        </li>
                        <li id="greeting">
                            <a href="Greeting.html">
                                <p>ご挨拶</p>
                                <p class="panel-caption">Greeting</p>
                            </a>
                        </li>
                        <li id="service">
                            <a href="Service.html">
                                <p>サービス概要</p>
                                <p class="panel-caption">Service</p>
                            </a>
                        </li>
                        <li id="approach">
                            <a href="Approach.html">
                                <p>弊社の取り組み</p>
                                <p class="panel-caption">Approach</p>
                            </a>
                        </li>
                        <li id="company">
                            <a href="Company.html">
                                <p>会社情報</p>
                                <p class="panel-caption">Company</p>
                            </a>
                        </li>
                        <li id="contact">
                            <a href="Contact.html">
                                <p>問い合わせ</p>
                                <p class="panel-caption">Contact</p>
                            </a>
                        </li>
                        <li id="logout">
                            <a href="logout.html">
                                <p>ログアウト</p>
                                <p class="panel-caption">Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- メインナビゲーション -->
        </div>
    </header>
    <!-- / ヘッダー -->

    <!-- メイン画像 -->
    <div id="mainBanner" class="mainImg">
        <div class="inner">
            <img src="images/mainImg.jpg" alt="" width="930" height="290">
            <div class="sloganB">
                <h2>わたしたちの選んだ家具を使ってもらいたい</h2>
                <h3>わたしたちのお気に入りを、みなさんのお気に入りにしてもらうのがわたしたちの夢です</h3>
            </div>
        </div>
    </div>
    <!-- / メイン画像 -->

    <div id="wrapper">

        <!-- 4カラム -->
        <form action="confirm.jsp" method="post">
            <section class="gridWrapper">
            <c:forEach var="item" items="${itemList}" varStatus = "status" begin="1" end="5" step="1">
                <article class="grid">
                		<h3><c:out value="${item.getItem_name()}"/></h3>
                    <p class="img"><img src=".${item.getImage_url()}" width="220" height="220" alt="${item.getItem_name()}"></p>
                    <p>
                        <dl>
                            <dt>商品ID：</dt>
                            <dd><c:out value="${item.getItem_id()}"/></dd>
                            <dt>価格：</dt>
                            <dd><c:out value="${item.getPrice()}"/></dd>
                            <dt>サイズ：</dt>
                            <dd><c:out value="${item.getItem_size()}"/></dd>
                            <dt>在庫：</dt>
                            <dd><c:out value="${item.getQuantity_size()}"/></dd>
                            <dt>注文数：</dt>
                            <dd>
                                <input class="order-quantity" type="number" inputmode="numeric" name="orderQuantity[${status.count}]" size="30" max="10" min="0" maxlength="8" value="0" autocorrect="off">
                            </dd>
                        </dl>
                    </p>
                    <div class="gridEnd"></div>
                </article>
            </c:forEach>
            </section>




            <div class="submit-wrapper">
                <input class="submit-button" type="submit" value="注文を確認する" />
            </div>
        </form>
    </div>
    <!-- / WRAPPER -->

    <!-- フッター -->
    <footer id="footer">
        <div class="inner">
            <!-- 3カラム -->
            <section class="gridWrapper">
                <article class="grid">
                    <!-- ロゴ -->
                    <p class="logo"><a href="index.html">Yasui Furniture<br /><span>イケてる家具をお手頃な値段であなたに</span></a></p>
                    <!-- / ロゴ -->
                </article>
                <article class="grid">
                    <!-- 電話番号+受付時間 -->
                    <p class="tel">電話: <strong>012-3456-7890</strong></p>
                    <p>受付時間: 平日 AM 10:00 〜 PM 9:00</p>
                    <!-- / 電話番号+受付時間 -->
                </article>
                <article class="grid copyright">
                    Copyright(c) 2020 Yasui Furniture Inc. All Rights Reserved.
                </article>
            </section>
            <!-- / 3カラム -->
        </div>
    </footer>
    <!-- / フッター -->
</body>
</html>
