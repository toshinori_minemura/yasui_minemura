package yasui_MinemuraServlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.sun.jdi.connect.spi.Connection; 候補から選んだ。

/**
 * Servlet implementation class Listitem
 */
@WebServlet("/Listitem")
public class ListitemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListitemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType ("text/html;charset=UTF-8");


		ItemDAO yasui = new ItemDAO();
		try {
			Connection conn = yasui.connect();

			ArrayList<ItemVO> itemList = yasui.getAllItem(conn);

			/*request.setAttribute("キッチンテーブル_name", itemList.get(0).getItem_name());
			request.setAttribute("キッチンテーブル_id", itemList.get(0).getItem_id());*/

			request.setAttribute("itemList", itemList);
			//System.out.println(request.getAttribute(itemList.get(0).getImage_url()));
			//request.setAttribute("テーブル_商品名", itemList.get(0).getItem_name());

			/*for(ItemVO s : itemList) {
				request.setAttribute("商品ID", "s.getItem_id()");
				request.setAttribute("商品name", s.getItem_name());
				//System.out.println("商品ID"+s.getItem_id()+":商品名"+s.getItem_name());
			}*/
			request.getRequestDispatcher("/list.jsp").forward(request, response);
			response.getWriter().append("Served at: ").append(request.getContextPath());

			System.out.println(itemList.get(0).getImage_url());
		} catch (SQLException | NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}



	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);


	}

}
