package yasui_MinemuraServlet;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public class BaseDAO {

	protected Connection connect() throws SQLException, NamingException{

		DataSource ds = null; //DataSourceの参照変数
		Connection con = null;//Connectionオブジェクトの参照変数
		Context context = null;//コンテキストの生成
		String localName = "java:comp/env/jdbc/yasui";
		context = new InitialContext();
		ds = (DataSource)context.lookup(localName);
		con = ds.getConnection();
		return con;
	}

	protected void disconnect(Connection conn) throws SQLException{
		if(conn != null){
			conn.close();
		}
	}


}
