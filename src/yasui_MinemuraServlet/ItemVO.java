package yasui_MinemuraServlet;

//商品表の各列と、在庫表の在庫数の列を取得したときに、その内容を格納できるItemVOを作成しなさい
public class ItemVO {

	private String item_id;//商品表
	private String item_name;//商品表
	private String image_url;//商品表
	private String item_size;//商品表
	private int price;//商品表
	private int quantity;//在庫表

	//00000005ソファー/images/eyecatch5.jpg100x60x70999990

	public ItemVO() { //引数なしコンストラクタの作成
		//デフォルト値入れなくて良い。
	}

	//引数ありコンストラクタの生成
	public ItemVO(String item_id,String item_name ,String image_url,String item_size,
			int price,int quantity) {
		this.item_id = item_id;
		this.item_name = item_name;
		this.image_url = image_url;
		this.item_size = item_size;
		this.price = price;
		this.quantity = quantity;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_id() {
		return this.item_id;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getItem_name() {
		return this.item_name;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getImage_url() {
		return this.image_url;
	}

	public void setItem_size(String item_size) {
		this.item_size = item_size;
	}

	public String getItem_size() {
		return this.item_size;
	}

	public void setPrice(int price) {
		//NumberFormat nf1 = NumberFormat.getCurrencyInstance(Locale.JAPAN);
		this.price = price;
		//this.price = nf1.format(price);
	}



	public int getPrice() {
		return this.price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQuantity_size() {
		return this.quantity;
	}



}
