package yasui_MinemuraServlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IndexRedirectServlet3
 */
@WebServlet("/IndexCheckServlet")
public class IndexCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);


		HashMap<String,String> userMap = new HashMap<>();
		//登録済みユーザーを格納するHashMap 事前の登録者三人分。
		//userMap.put("admin", "password");
		//userMap.put("customer1", "password");
		//userMap.put("customer2", "password");
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		String error = null;
		HttpSession session = request.getSession(true);

		BaseDAO baseDao = new BaseDAO();
		UserCheckDAO checkDao = new UserCheckDAO();

		Connection con = null;
		try {
			con = baseDao.connect();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		//送信されたユーザー名とパスワードが空だったら
		//userName==null||password==null||userName.isBlank()||password.isBlank()←元々のチェック条件
				/*try {
					if(checkDao.getUserName(con, userName, password)) {
						error="ユーザー名、またはパスワードが正しく送信されていません";
						response.sendRedirect("loginerror.jsp");
						return;
					}else {
						request.getRequestDispatcher("ListitemServlet").forward(request, response);
						return;
					}
				} catch (SQLException | IOException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				/*if(userMap.containsKey(userName)&&userMap.get(userName).equals(password)) {
					session.setAttribute("username", userName);
				}else {
					error="ユーザー名、またはパスワードが正しくありません";
				}*/

				//なんらかのエラーがあったらエラーをセッションに格納する
				try {
					if(!checkDao.getUserName(con, userName, password)) {
						session.setAttribute("error", error);
						response.sendRedirect("loginerror.jsp");
						//return;
					}else {
						request.getRequestDispatcher("Listitem").forward(request, response);
					//response.sendRedirect("ListitemServlet");
					//return
}
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				} catch (IOException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				} catch (ServletException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}

	}
}


