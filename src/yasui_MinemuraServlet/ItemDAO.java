package yasui_MinemuraServlet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ItemDAO extends BaseDAO{

	//•演習1-1相当の処理を行う　OK??
	public ArrayList<ItemVO> getAllItem(Connection con)throws SQLException{//try chatch入らない。

			String query = "select i.item_id,i.item_name,i.image_url,i.item_size,i.price, "
							+ "s.quantity,i.is_delete "
							+ "from items i inner join stocks s on i.item_id = s.item_id;";

			/*Connection conn = null;
			YasuiDAO yasui = new YasuiDAO();
			conn = yasui.connect();*///↓↑接続した、conを受け取るので接続作業は不要、queryの作業は必要。
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);//rsの中に表の情報が入っている。
													//ex) rs=1 には各レコードが入る。
			ArrayList<ItemVO> list = new ArrayList<ItemVO>();//ItemVOのlistには item_id, item_name,
									//image_url, item_size, price, quantityが要素として入れられる。
			while(rs.next()) {//rs.next()がtrueの間（＝表のレコードが存在している間）繰り返す。

				ItemVO vo = new ItemVO();

			vo.setItem_id(rs.getString("item_id"));
			vo.setItem_name(rs.getString("item_name"));
			vo.setImage_url(rs.getString("image_url"));
			vo.setItem_size(rs.getString("item_size"));
			vo.setPrice(rs.getInt("price"));
			vo.setQuantity(rs.getInt("quantity"));

			 list.add(vo);
			}
			return list;
	   }

	public ItemVO searchItem(Connection con,String item_name) throws SQLException {
		String query = "select i.item_id,i.item_name,i.image_url,i.item_size,i.price, "
				+ "s.quantity,i.is_delete "
				+ "from items i inner join stocks s on i.item_id = s.item_id"
				+ " where item_name = '"+item_name+"';";

		/*Connection conn = null;
		YasuiDAO yasui = new YasuiDAO();
		conn = yasui.connect();*///↓↑接続した、conを受け取るので接続作業は不要、queryの作業は必要。
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(query);//rsの中に表の情報が入っている。
												//ex) rs=1 には各レコードが入る。

		//ArrayList<ItemVO> list = new ArrayList<ItemVO>();//ItemVOのlistには item_id, item_name,
								//image_url, item_size, price, quantityが要素として入れられる。
		ItemVO vo = new ItemVO();
		while(rs.next()) {//rs.next()がtrueの間（＝表のレコードが存在している間）繰り返す。

			//ItemVO vo = new ItemVO();
		vo.setItem_id(rs.getString("item_id"));
		vo.setItem_name(rs.getString("item_name"));
		vo.setImage_url(rs.getString("image_url"));
		vo.setItem_size(rs.getString("item_size"));
		vo.setPrice(rs.getInt("price"));
		vo.setQuantity(rs.getInt("quantity"));
		return vo;//voが帰ってきたら中身は必ずある。

		}
		return  null;//からのvoではなく敢えてnullを返す。

	}








		//演習1-2相当の処理を行う
	public void addItem(Connection con,ItemVO vo) throws SQLException  {//使うときは、voの中にインサートしたい商品名などを入れて呼び出す。
			//ここでやりたいのは、引数vo　に応じて機能するメソッドの作成！
			String query = "INSERT INTO items VALUES (?,?,?,?,?,false)";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, vo.getItem_id());
			stmt.setString(2, vo.getItem_name());
			stmt.setString(3, vo.getImage_url());
			stmt.setString(4,vo.getItem_size());
			stmt.setInt(5, vo.getPrice());


			//インサート分は''で囲む。
			stmt.executeUpdate();//int型　戻り値=更新行数(int型)
			//↑countには1 が入り、DBに挿入した行が入っている。

			String query1 = "INSERT INTO stocks VALUES (?,0,false)";
			PreparedStatement stmt1 = con.prepareStatement(query1);
			stmt1.setString(1, vo.getItem_id());



			//インサート分は''で囲む。
			stmt1.executeUpdate();//int型　戻り値=更新行数(int型)

		}


	//演習1-3相当の処理を行う
	public void updateStock(Connection con,String item_id,int newStock) throws SQLException{
			//item_id と newStock に入れた値で更新できるようにする。
		    String query  = "update stocks set quantity = ? where item_id = ?";
			PreparedStatement stmt = con.prepareStatement(query);

			stmt.setInt(1, newStock);
			stmt.setString(2,item_id);
			//Statement stmt = con.createStatement();
			//String query  = "update stocks set quantity = 50 where item_id = '00000006';";
			//↑この段階では、引数に関係ない処理だから呼び出してもうまく使えない。
			//item_idが000000006のところを見つけ出し、そこにgetterを使って表にsetすれば良い。
			//update テーブル名 set 列名1 = 値1, 列名2 = 値2
								//where 抽出する条件


			 stmt.executeUpdate();
	}


	//演習1-4相当の処理を行う
	public void deleteItem(Connection con,String item_id) throws SQLException{

			Statement stmt = con.createStatement();
			String query  = "delete from stocks where item_id = "+item_id+";";
			int count = stmt.executeUpdate(query);

	}






}
